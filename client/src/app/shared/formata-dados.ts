export class FormataDados {

    static removeMascaraCpf(cpf: any) {
        return cpf.replaceAll('.', '').replaceAll('-', '');
    }

    static removeMascaraCNPJ(cnpj: any) {
        return cnpj.replaceAll('.', '').replaceAll('-', '');
    }

    static formataMascaraCpf() { }
    static formataMascaraCNJP() { }

}