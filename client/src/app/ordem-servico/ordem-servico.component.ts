import { Component, OnInit } from '@angular/core';
import { MatDialog } from '@angular/material/dialog';
import { CadastroOrdemServicoComponent } from './cadastro-ordem-servico/cadastro-ordem-servico.component';
import { OrdemServicoService } from './services/ordem-servico.service';


export interface OrdemServico {
  Titulo: string;
  CNPJ: string;
  NomeCliente: string;
  NomePrestador: string;
  CpfPrestador: string;
  DataExecucao: Date;
  Valor: number
}

@Component({
  selector: 'app-ordem-servico',
  templateUrl: './ordem-servico.component.html',
  styleUrls: ['./ordem-servico.component.css']
})
export class OrdemServicoComponent implements OnInit {

  constructor(private ordemServicoService: OrdemServicoService, public dialog: MatDialog) { }

  OrdensColumns: string[] = ['Id', 'Titulo', 'CNPJ', 'NomeCliente', 'NomePrestador', 'CpfPrestador', 'dataExecucao', 'Valor', 'Editar', 'Deletar'];
  dataSource = [];

  ngOnInit(): void {
    this.ListaOrdemServicos();
  }

  DeletarArquivo(ordem: any) {
    console.log(ordem);
    this.ordemServicoService.DeletarOrdemServico(ordem.id).subscribe(x => {
      this.ListaOrdemServicos();
    });
  }

  EditarOrdemServico(ordem: any) {
    const dialogRef = this.dialog.open(CadastroOrdemServicoComponent, {
      height: '600px',
      width: '500px',
      data: { ordem }
    });

    dialogRef.afterClosed().subscribe(result => {
      this.ListaOrdemServicos();
    });
  }

  ListaOrdemServicos() {
    this.ordemServicoService.ListaOrdemServico().subscribe((retorno: any) => {

      retorno.map((str) => {
        str.cpfPrestador = str.cpfPrestador.replace(/(\d{3})(\d{3})(\d{3})(\d{2})/g, "\$1.\$2.\$3\-\$4");
        str.cnpj = str.cnpj.replace(/(\d{2})(\d{3})(\d{3})(\d{4})(\d{2})/g, "\$1.\$2.\$3\/\$4\-\$5");
        
        //var dataFormatada = str.dataExecucao.replace(/(\d*)-(\d*)-(\d*).*/, '$3-$2-$1');
        // return { cpf: str.cpfPrestador, cnpj: str.cnpj }
      });

      this.dataSource = retorno;
    });
  }

  openDialog() {
    const dialogRef = this.dialog.open(CadastroOrdemServicoComponent, {
      height: '600px',
      width: '500px'
    });

    dialogRef.afterClosed().subscribe(result => {
      this.ListaOrdemServicos();
    });
  }
}
