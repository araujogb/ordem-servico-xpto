import { Component, Inject, OnInit } from '@angular/core';
import { FormControl, FormGroup, Validators } from '@angular/forms';
import { MatDialogRef } from '@angular/material/dialog';
import { OrdemServicoService } from '../services/ordem-servico.service';
import { MAT_DIALOG_DATA } from '@angular/material/dialog';
import { Validator } from 'src/app/shared/validador';

@Component({
  selector: 'app-cadastro-ordem-servico',
  templateUrl: './cadastro-ordem-servico.component.html',
  styleUrls: ['./cadastro-ordem-servico.component.css']
})
export class CadastroOrdemServicoComponent implements OnInit {

  formCadastro: FormGroup
  id: number;

  constructor(private dialogRef: MatDialogRef<CadastroOrdemServicoComponent>,
    private ordemServicoService: OrdemServicoService,
    @Inject(MAT_DIALOG_DATA) public data: any) { }

  ngOnInit(): void {
    this.criaUsuarioForm();

    if (this.data) {
      this.id = this.data.ordem.id;

      this.formCadastro.patchValue({
        "Titulo": this.data.ordem.titulo,
        "NomeCliente": this.data.ordem.nomeCliente,
        "CNPJ": this.data.ordem.cnpj,
        "NomePrestador": this.data.ordem.nomePrestador,
        "CpfPrestador": this.data.ordem.cpfPrestador,
        // "DataExecucao": this.data.ordem.dataExecucao,        
        "DataExecucao": this.FormataData(this.data.ordem.dataExecucao),
        "Valor": this.data.ordem.valor
      });
    }
  }

  FormataData(data) {
    var dataArray = data.split("-");
    return dataArray[0] + "-" + dataArray[1] + "-" + dataArray[2].substring(0, 2);
  }

  public Cadastrar() {
    var OS = this.formCadastro.value;
    OS.id = this.id;

    OS.CpfPrestador = OS.CpfPrestador.replaceAll('.', '').replaceAll('-', '');
    OS.CNPJ = OS.CNPJ.replaceAll('.', '').replaceAll('-', '').replaceAll('/', '');

    if (this.id) {
      this.ordemServicoService.AtualizaOrdemServico(OS, this.id).subscribe((retorno: any) => {
        this.dialogRef.close();
      });
    } else {
      this.ordemServicoService.adicionarOrdemServico(OS).subscribe((retorno: any) => {
        if (retorno.hasError == false) {
          this.dialogRef.close();
        }
      });
    }
  }

  public criaUsuarioForm() {
    this.formCadastro = new FormGroup({
      "Titulo": new FormControl(null, Validators.required),
      "NomeCliente": new FormControl(null, Validators.required),
      "CNPJ": new FormControl(null, [Validators.required, Validator.ValidaCNPJ]),
      "NomePrestador": new FormControl(null, Validators.required),
      "CpfPrestador": new FormControl(null, [Validators.required, Validator.ValidaCpf]),
      "DataExecucao": new FormControl(null, Validators.required),
      "Valor": new FormControl(null, Validators.required),
    });
  }
}