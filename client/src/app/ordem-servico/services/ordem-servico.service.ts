import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { environment } from 'src/environments/environment';

@Injectable({
  providedIn: 'root'
})
export class OrdemServicoService {

  private readonly url: string;

  constructor(private http: HttpClient) {
    this.url = environment.backendUrlApi
  }

  public ListaOrdemServico() {
    return this.http.get(this.url + "OrdemServico");
  }

  public adicionarOrdemServico(body: any) {
    console.log(body);
    return this.http.post(this.url + "OrdemServico", body);
  }

  public DeletarOrdemServico(body: any) {
    console.log(body);
    return this.http.delete(this.url + "OrdemServico/" + body);
  }

  public AtualizaOrdemServico(body: any,id: number) {
    return this.http.put(this.url + "OrdemServico", body);
    // return this.http.put(this.url + "OrdemServico/" + id, body);
  }

}
