﻿using OrdemServicoXPTO.core.Models;
using OrdemServicoXPTO.core.Service.Dto;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace OrdemServicoXPTO.core.Interfaces.Repositories
{
    public interface IOrdemServicoRepository
    {
        public Task<int> SalvaOrdemServicoAsync(OrdemServico ordemServico);
        public Task<int> AtualizaOrdemServicoAsync(OrdemServico ordemServico);
        public Task<OrdemServico> ProcuraOrdemServicoAsync(int id);
        public Task<List<OrdemServico>> RetonaOrdemServicosAsync();
        public Task<bool> DeletaOrdemServicosAsync(OrdemServico ordemServico);
    }
}
