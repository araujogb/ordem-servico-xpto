﻿using OrdemServicoXPTO.core.Dto;
using OrdemServicoXPTO.core.Models;
using OrdemServicoXPTO.core.Service.Dto;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace OrdemServicoXPTO.core.Interface.Services
{
    public interface IOrdemServicoApplication
    {
        public Task<ResponseDto> AdicionarOrdemServico(OrdemServicoDto ordemServico);
        public Task<List<OrdemServico>> RetornaOrdemServico();
        public Task<OrdemServico> RetornaOrdemServicoId(int Id);
        public Task<int> AtualizaOrdemServico(OrdemServicoDto ordemServico);
        public Task<bool> DeletaOrdemServico(int Id);
    }
}
