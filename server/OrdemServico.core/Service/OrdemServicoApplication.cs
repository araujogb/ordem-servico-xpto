﻿using AutoMapper;
using OrdemServicoXPTO.core.Dto;
using OrdemServicoXPTO.core.Interface.Services;
using OrdemServicoXPTO.core.Interfaces.Repositories;
using OrdemServicoXPTO.core.Models;
using OrdemServicoXPTO.core.Service.Dto;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace OrdemServicoXPTO.core.Service
{
    public class OrdemServicoApplication : IOrdemServicoApplication
    {
        private readonly IOrdemServicoRepository ordemServicoRepository;
        private readonly IMapper _mapper;

        public OrdemServicoApplication(IOrdemServicoRepository ordemServicoRepository, IMapper mapper)
        {
            this.ordemServicoRepository = ordemServicoRepository;
            this._mapper = mapper;
        }

        public async Task<ResponseDto> AdicionarOrdemServico(OrdemServicoDto ordemServico)
        {
            try
            {
                var ordem = _mapper.Map<OrdemServico>(ordemServico);
                var retorno = await ordemServicoRepository.SalvaOrdemServicoAsync(ordem);
                return new ResponseDto { HasError = false, StatusCode = 200, Content = retorno };
            }
            catch (System.Exception e)
            {
                return new ResponseDto { HasError = true, StatusCode = 500, Content = e.Message };
            }

        }

        public async Task<List<OrdemServico>> RetornaOrdemServico()
        {
            return await ordemServicoRepository.RetonaOrdemServicosAsync();
        }

        public async Task<OrdemServico> RetornaOrdemServicoId(int Id)
        {
            return await ordemServicoRepository.ProcuraOrdemServicoAsync(Id);
        }

        public async Task<int> AtualizaOrdemServico(OrdemServicoDto ordemServico) 
        {
            var ordem = _mapper.Map<OrdemServico>(ordemServico);
            return await ordemServicoRepository.AtualizaOrdemServicoAsync(ordem);
        }

        public async Task<bool> DeletaOrdemServico(int Id)
        {
            var os = await ordemServicoRepository.ProcuraOrdemServicoAsync(Id);

            if (os != null) 
            {
                return await ordemServicoRepository.DeletaOrdemServicosAsync(os);
            }

            return false;
        }


    }
}
