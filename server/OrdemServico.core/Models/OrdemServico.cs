﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace OrdemServicoXPTO.core.Models
{
    public class OrdemServico
    {
        public int Id { get; set; }
        public string Titulo { get; set; }
        public string CNPJ { get; set; }
        public string NomeCliente { get; set; }
        public string NomePrestador { get; set; }
        public string CpfPrestador { get; set; }
        public DateTime DataExecucao { get; set; }
        public double Valor { get; set; }

        //public string CpfFormtado
        //{
        //    get { return FormatarCpf(); }
        //    set { CpfPrestador = value; }
        //}

        //private string FormatarCpf()
        //{
        //    var dg = CpfPrestador.Substring(9, 2);
        //    var prt1 = CpfPrestador.Substring(0, 3);
        //    var prt2 = CpfPrestador.Substring(3, 3);
        //    var prt3 = CpfPrestador.Substring(6, 3);
        //    return $"{prt1}.{prt2}.{prt3}-{dg}";
        //}

        //public string cnpj { get; set; }

        //public string CNPJ
        //{
        //    get { return FormataCNPJ(); }
        //    set { cnpj = value; }
        //}

        //private string FormataCNPJ()
        //{
        //    var dg = cnpj.Substring(12, 2);
        //    var prt4 = cnpj.Substring(9, 4);
        //    var prt3 = cnpj.Substring(5, 3);
        //    var prt2 = cnpj.Substring(2, 3);
        //    var prt1 = cnpj.Substring(0, 2);

        //    return $"{prt1}.{prt2}.{prt3}/{prt4}-{dg}";
        //}
    }
}
