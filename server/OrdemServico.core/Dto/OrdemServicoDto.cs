﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace OrdemServicoXPTO.core.Service.Dto
{
    public class OrdemServicoDto
    {
        public int Id { get; set; }
        public string Titulo { get; set; }
        public string CNPJ { get; set; }
        public string NomeCliente { get; set; }
        public string NomePrestador { get; set; }
        public string CpfPrestador { get; set; }
        public DateTime DataExecucao { get; set; }
        public double Valor { get; set; }
    }
}
