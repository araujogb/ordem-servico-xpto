﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace OrdemServicoXPTO.core.Dto
{
    public class ResponseDto
    {
        public bool HasError { get; set; }
        public int StatusCode { get; set; }
        public object Content { get; set; }
    }
}
