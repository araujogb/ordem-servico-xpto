﻿using Microsoft.AspNetCore.Mvc;
using OrdemServicoXPTO.core.Dto;
using OrdemServicoXPTO.core.Interface.Services;
using OrdemServicoXPTO.core.Models;
using OrdemServicoXPTO.core.Service.Dto;
using System.Collections.Generic;
using System.Threading.Tasks;

// For more information on enabling Web API for empty projects, visit https://go.microsoft.com/fwlink/?LinkID=397860

namespace OrdemServicoXPTO.api.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class OrdemServicoController : ControllerBase
    {
        private readonly IOrdemServicoApplication ordemServicoApplicatio;

        public OrdemServicoController(IOrdemServicoApplication ordemServicoApplicatio)
        {
            this.ordemServicoApplicatio = ordemServicoApplicatio;
        }

        [HttpGet]
        public async Task<List<OrdemServico>> Get()
        {
            return await ordemServicoApplicatio.RetornaOrdemServico();
        }

        [HttpGet("{id}")]
        public async Task<OrdemServico> Get(int id)
        {
            return await ordemServicoApplicatio.RetornaOrdemServicoId(id);
        }

        [HttpPost]
        public async Task<ResponseDto> Post([FromBody] OrdemServicoDto ordemServico)
        {
            return await ordemServicoApplicatio.AdicionarOrdemServico(ordemServico);
        }

        [HttpPut]
        public async Task<int> Put([FromBody] OrdemServicoDto ordemServico)
        {
            return await ordemServicoApplicatio.AtualizaOrdemServico(ordemServico);
        }

        [HttpDelete("{id}")]
        public async Task<bool> Delete(int id)
        {
            return await ordemServicoApplicatio.DeletaOrdemServico(id);
        }
    }
}
