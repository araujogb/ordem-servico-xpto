﻿using Microsoft.EntityFrameworkCore;
using OrdemServicoXPTO.core.Interfaces.Repositories;
using OrdemServicoXPTO.core.Models;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace OrdemServicoXPTO.infrastructure.Repositories
{
    public class OrdemServicoRepository : IOrdemServicoRepository
    {
        private readonly OrdemServicoContext ordemServicoContext;

        public OrdemServicoRepository(OrdemServicoContext ordemServicoContext)
        {
            this.ordemServicoContext = ordemServicoContext;
        }

        public async Task<int> SalvaOrdemServicoAsync(OrdemServico ordemServico)
        {
            ordemServicoContext.OrdemServicos.Add(ordemServico);
            await ordemServicoContext.SaveChangesAsync();
            return ordemServico.Id;
        }

        public async Task<int> AtualizaOrdemServicoAsync(OrdemServico ordemServico)
        {
            ordemServicoContext.OrdemServicos.Update(ordemServico);
            await ordemServicoContext.SaveChangesAsync();
            return ordemServico.Id;
        }

        public async Task<OrdemServico> ProcuraOrdemServicoAsync(int Id)
        {
            return await ordemServicoContext.OrdemServicos.FirstOrDefaultAsync(x => x.Id == Id);
        }

        public async Task<List<OrdemServico>> RetonaOrdemServicosAsync()
        {
            return await ordemServicoContext.OrdemServicos.AsNoTracking().ToListAsync();
        }

        public async Task<bool> DeletaOrdemServicosAsync(OrdemServico ordemServico)
        { 
            ordemServicoContext.OrdemServicos.Remove(ordemServico);
            var retorno = await ordemServicoContext.SaveChangesAsync();

            if (retorno > 0)
                return true;

            return false;
        }
    }
}
