﻿using Microsoft.EntityFrameworkCore;
using OrdemServicoXPTO.core.Models;
using OrdemServicoXPTO.infrastructure.TypeConfiguration;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace OrdemServicoXPTO.infrastructure
{
    public  class OrdemServicoContext : DbContext
    {
        public OrdemServicoContext(DbContextOptions<OrdemServicoContext> options) : base(options)
        {
        }

        public DbSet<OrdemServico> OrdemServicos { get; set; }

        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            base.OnModelCreating(modelBuilder);
            modelBuilder.ApplyConfiguration(new OrdemServicoConfiguration());
        }
    }
}
