﻿using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;
using OrdemServicoXPTO.core.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace OrdemServicoXPTO.infrastructure.TypeConfiguration
{
    public class OrdemServicoConfiguration : IEntityTypeConfiguration<OrdemServico>
    {
        public void Configure(EntityTypeBuilder<OrdemServico> builder)
        {
            builder.ToTable("OrdemServicos");
            builder.HasKey(x => x.Id);
            builder.Property(x => x.Titulo);
            builder.Property(x => x.CNPJ);
            builder.Property(x => x.NomeCliente);
            builder.Property(x => x.NomePrestador);
            builder.Property(x => x.CpfPrestador);
            builder.Property(x => x.DataExecucao);
            builder.Property(x => x.Valor);
            //builder.Ignore(x => x.CpfFormtado);
        }
    }
}
